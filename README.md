# Gaussian Location-Scale Regression Models

The Gaussian location-scale regression model is a multi-predictor
model with explanatory variables for the mean (= location) and the standard
deviation (= scale) of a response variable. This package implements maximum
likelihood and Markov chain Monte Carlo (MCMC) inference, a parametric
bootstrap algorithm, and diagnostic plots for the model.
